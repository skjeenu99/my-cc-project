const multer = require("multer");
const express = require("express");
const { SQSClient, SendMessageCommand, ReceiveMessageCommand, DeleteMessageCommand, GetQueueAttributesCommand, } = require("@aws-sdk/client-sqs");
const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");
const { EC2Client, StartInstancesCommand, StopInstancesCommand, DescribeInstancesCommand, } = require("@aws-sdk/client-ec2");
const { fromIni } = require("@aws-sdk/credential-provider-ini");

const app = express();
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const sqs = new SQSClient({ credentials: fromIni({ profile: "default" }), });

const s3 = new S3Client({ credentials: fromIni({ profile: "default" }), });

const ec2 = new EC2Client({ profile: "default" });

const requrl = "https://sqs.us-east-1.amazonaws.com/767398095965/1229701072-req-queue";
const bucketIn = "1229701072-in-bucket";
const resurl = "https://sqs.us-east-1.amazonaws.com/767398095965/1229701072-resp-queue";
const bucketOut = "1229701072-out-bucket";

const instanceIds = [
  "i-0d1217cb72b1ef996",
  "i-0200bf899cfcea483",
  "i-057a8e2e44c9d51a1",
  "i-06829ec6c4050ec7b",
  "i-0c0d07e39a2406060",
  "i-02f2e5176a14673f5",
  "i-0ac8a5646656bf969",
  "i-0401dc97b4a53da64",
  "i-0d2aee7cffcee8c88",
  "i-000748fabba8d5db7",
  "i-07fcca35f78c2360e",
  "i-0845d4fcc17e5e738",
  "i-02b43049412c168c5",
  "i-081cafa9fe0c8b31e",
  "i-00d545581ac47b4c9",
  "i-0036e394f61c5249a",
  "i-09fbbbdf210c0cdf4",
  "i-08e119f7a1dcbda5e",
  "i-03669f5880ddc8131"
];

async function getCurrentInstanceState() {
  const qry = new DescribeInstancesCommand({
    InstanceIds: instanceIds,
  });

  const response = await ec2.send(qry);
  const startInst = [];
  const stopInst = [];
  const upInst = [];
  const downInst = [];

  response.Reservations.forEach((reservation) => {
    reservation.Instances.forEach((inst) => {
      const state = inst.State.Name;
      switch (state) {
        case "running":
          upInst.push(inst.InstanceId);
          break;
        case "pending":
          upInst.push(inst.InstanceId);
          break;
        case "stopped":
          startInst.push(inst.InstanceId);
          break;
        case "stopping":
        case "shutting-down":
          downInst.push(inst.InstanceId);
          break;
      }
    });
  });

  stopInst.push(...upInst);

  return { startInst, stopInst, upInst, downInst, };
}

async function getMsgNum() {
  const qry = new GetQueueAttributesCommand({
    QueueUrl: requrl,
    AttributeNames: ["ApproximateNumberOfMessages"],
  });

  const response = await sqs.send(qry);
  const availableMessages = parseInt(
    response.Attributes.ApproximateNumberOfMessages,
    10
  );

  return availableMessages;
}

async function repropEC2(targetRunningInstances) {
  const {
    startInst,
    stopInst,
    upInst,
    downInst,
  } = await getCurrentInstanceState();

  const currentRunningCount = upInst.length;
  const instancesNeeded = targetRunningInstances - currentRunningCount;

  if (instancesNeeded > 0) {
    const instancesToActuallyStart = startInst.slice(0, instancesNeeded);
    if (instancesToActuallyStart.length > 0) {
      try {
        await ec2.send(new StartInstancesCommand({ InstanceIds: instancesToActuallyStart }));
        console.log(`Started instances: ${instancesToActuallyStart.join(", ")}`);
      }
      catch (error) {
        console.error(`Error starting instances: ${error}`);
      }
    }
  }
  else if (targetRunningInstances === 0) {
    const instancesToActuallyStop = stopInst.slice(0, Math.abs(instancesNeeded));
    if (instancesToActuallyStop.length > 0) {
      try {
        await ec2.send(new StopInstancesCommand({ InstanceIds: instancesToActuallyStop }));
        console.log(`Stopped instances: ${instancesToActuallyStop.join(", ")}`);
      }
      catch (error) {
        console.error(`Error stopping instances: ${error}`);
      }
    }
  }
}

async function manageInstances() {
  let maxRequiredInst = 0;
  let messages = await getMsgNum();
  console.log("Initial message count:" + messages);
  if (messages === 0) {
    console.log("Double-checking message count after initial count of 0.");
    await new Promise((resolve) => setTimeout(resolve, 3000));
    messages = await getMsgNum();
    console.log("Message count after double-check:" + messages);
  };

  if (messages <= 10) {
    maxRequiredInst = messages;
  }
  else {
    maxRequiredInst = Math.min(Math.ceil((messages - 10) / 4) + 10, 20);
  };

  await repropEC2(maxRequiredInst);
}

setInterval(manageInstances, 5 * 1000);

async function pollResQ() {
  const receiveParams = {
    QueueUrl: resurl,
    MaxNumberOfMessages: 10,
    WaitTimeSeconds: 20,
  };

  while (true) {
    try {
      const { Messages } = await sqs.send(new ReceiveMessageCommand(receiveParams));
      if (Messages) {
        for (const message of Messages) {
          const { fileName, result } = JSON.parse(message.Body);
          console.log(fileName, result);
          if (clssfnPndg[fileName]) {
            clssfnPndg[fileName].resolve(result);
          }
          const deleteParams = {
            QueueUrl: resurl,
            ReceiptHandle: message.ReceiptHandle,
          };
          await sqs.send(new DeleteMessageCommand(deleteParams));
        }
      }
    }
    catch (err) {
      console.error("Error receiving messages from SQS:", err);
    }
  }
}

pollResQ();

const clssfnPndg = {};

app.post("/", upload.single("inputFile"), async (req, res) => {
  if (!req.file) {
    return res.status(400).send("No file uploaded.");
  }

  const fileContent = req.file.buffer;
  const fileName = req.file.originalname;

  const uploadParams = {
    Bucket: bucketIn,
    Key: fileName,
    Body: fileContent,
  };

  try {
    await s3.send(new PutObjectCommand(uploadParams));
    console.log("File is Uploaded!");
  }
  catch (err) {
    console.error("Unable to upload file: ", err);
    return res.status(500).send("Unable to upload file!");
  }

  const sqsParams = { QueueUrl: requrl, MessageBody: JSON.stringify({ fileName: fileName, imageData: fileContent.toString("base64"), }) };

  try {
    await sqs.send(new SendMessageCommand(sqsParams));
    console.log("Message sent to SQS successfully.");
  }
  catch (err) {
    console.error("Error sending message to SQS: ", err);
    return res.status(500).send("Error sending message to SQS.");
  }

  let resolveFn, rejectFn;
  const promise = new Promise((resolve, reject) => {
    resolveFn = resolve;
    rejectFn = reject;
  });
  clssfnPndg[fileName] = { resolve: resolveFn, reject: rejectFn };

  try {
    const resultclssfn = await promise;
    console.log(resultclssfn);
    res.send(`${fileName}:${resultclssfn}`);
  } catch (error) {
    res.status(500).send("Error processing file.");
  } finally {
    delete clssfnPndg[fileName];
  }
});


// Starting Server
app.listen(8000, () => {
  console.log("Server listening on port 8000");
});
