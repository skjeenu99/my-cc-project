const path = require("path");
const fs = require("fs");

const { exec } = require("child_process");
const { SQSClient, SendMessageCommand, ReceiveMessageCommand, DeleteMessageCommand, } = require("@aws-sdk/client-sqs");
const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");
const { fromIni } = require("@aws-sdk/credential-provider-ini");


const sqs = new SQSClient({ credentials: fromIni({ profile: "default" }), });
const s3 = new S3Client({ credentials: fromIni({ profile: "default" }), });

const requrl = "https://sqs.us-east-1.amazonaws.com/767398095965/1229701072-req-queue";
const bucketIn = "1229701072-in-bucket";
const resurl = "https://sqs.us-east-1.amazonaws.com/767398095965/1229701072-resp-queue";
const bucketOut = "1229701072-out-bucket";

const homeDir = "home";
const modelDir = path.join(homeDir, "model");
const appDir = path.join(homeDir, "app");
const tempDir = path.join(appDir, "temp");

if (!fs.existsSync(tempDir)) {
  fs.mkdirSync(tempDir);
}

function msgPoll() {
  try {
    const receiveParams = { QueueUrl: requrl, MaxNumberOfMessages: 1, WaitTimeSeconds: 20, };
    sqs.send(new ReceiveMessageCommand(receiveParams)).then((received) => {
      if (received.Messages && received.Messages.length > 0) {
        const message = received.Messages[0];
        const body = JSON.parse(message.Body);
        const { fileName, imageData } = body;
        const tempFilePath = path.join(tempDir, fileName);

        fs.writeFileSync(tempFilePath, Buffer.from(imageData, "base64"));
        const pythonScriptPath = path.join(modelDir, "face_recognition.py");

        exec(
          `python ${pythonScriptPath} ${tempFilePath}`,
          async (error, stdout, stderr) => {
            if (error) {
              console.error(`exec error: ${error}`);
              return;
            }
            console.log(`Recognition result: ${stdout.trim()}`);

            const recognitionResult = stdout.trim();
            const resultKey = fileName.split(".").slice(0, -1).join(".");
            await s3.send(
              new PutObjectCommand({
                Bucket: bucketOut,
                Key: resultKey,
                Body: recognitionResult,
              })
            );

            console.log(`Result stored in S3 with key ${resultKey}`);

            const responseMessage = {
              fileName: resultKey + ".jpg",
              result: recognitionResult,
            };

            await sqs.send(new SendMessageCommand({ QueueUrl: respurl, MessageBody: JSON.stringify(responseMessage), }));
            console.log("Result sent to the response queue.");

            sqs.send(new DeleteMessageCommand({ QueueUrl: requrl, ReceiptHandle: message.ReceiptHandle, }))
              .then(() => {
                fs.unlinkSync(tempFilePath);
                setImmediate(msgPoll);
              });
          }
        );
      }
      else {
        console.log("No messages to process.");
        setTimeout(msgPoll, 10000);
      }
    });
  }
  catch (error) {
    console.error("Error:", error);
    setTimeout(msgPoll, 10000);
  }
}

msgPoll();