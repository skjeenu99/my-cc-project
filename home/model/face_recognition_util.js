import { exec } from "child_process";
import { promisify } from "util";
import logger from "../logger.js";

const execPromise = promisify(exec);

async function predictFace(imageFileName) {
  try {
    logger.info("Running face recognition script on image:", imageFileName);
    const { stdout, stderr } = await execPromise(
      `cd face_recognition && python face_recognition.py ../${imageFileName}`
    );

    if (stderr.trim()) {
      logger.error("Error occurred while running face recognition script:", stderr);
    }

    return stdout.trim();
  } catch (error) {
    logger.error("Error occurred while executing face recognition :", error);
    throw error;
  }
}

export { predictFace };
